import { componentScriptLoader } from "@logic/client/clientNavigation.js";

export default function registerDarkModeSwitch() {
  componentScriptLoader("#darkModeSwitch", () => {
    function setDarkMode(darkTheme) {
      const el = document.querySelector("body");
      if (darkTheme) {
        localStorage.setItem("darkMode", true);
        el.classList.add("theme-dark");
      } else {
        localStorage.setItem("darkMode", false);
        el.classList.remove("theme-dark");
      }
    }

    setDarkMode(
      localStorage.getItem("darkMode") === null
        ? window.matchMedia &&
            window.matchMedia("(prefers-color-scheme: dark)").matches
        : !!(localStorage.getItem("darkMode") == "true")
    );

    document.getElementById("darkModeSwitch").onclick = () => {
      setDarkMode(!(localStorage.getItem("darkMode") == "true"));
    };
  });
}
