import slugify from "@sindresorhus/slugify";

import releasesData from "@data/releases.json";
import * as dataUtils from "./dataUtils.js";

const ansiCodes = {
  yellowFG: "\x1b[33m",
  redFG: "\x1b[31m",
  greenFG: "\x1b[32m",
  reset: "\x1b[0m"
};

const mergeFunctions = {
  variants: {
    filePath: () => {},
    price: mergeWithObjectAssign,
    releases: mergeWithDirectAssign,
    warnings: mergeWithDirectAssign,
    portStatus: mergePortStatus,
    deviceInfo: mergeDeviceInfo,
    contributors: mergeWithConcatenation,
    communityHelp: mergeWithConcatenation,
    docLinks: mergeWithConcatenation,
    externalLinks: mergeWithConcatenation,
    sources: mergeWithObjectAssign,
    seo: mergeWithObjectAssign,
    content: mergeContent,
    defaultMergeMethod: verifiedDirectMerge
  },
  releases: {
    filePath: () => {},
    name: () => {},
    deviceType: () => {},
    price: () => {},
    variantOf: () => {},
    portStatus: mergeWithDirectAssign,
    deviceInfo: () => {},
    contributors: mergeWithConcatenation,
    communityHelp: mergeWithConcatenation,
    docLinks: mergeWithConcatenation,
    externalLinks: mergeWithConcatenation,
    sources: mergeWithDirectAssign,
    seo: mergeWithObjectAssign,
    content: mergeWithDirectAssign,
    defaultMergeMethod: verifiedDirectMerge
  }
};

export function mergeReleases(devices, releases) {
  let mergedDevices = [];

  devices.forEach((device) => {
    let deviceReleases = releases.filter((r) => r.codename == device.codename);
    let variantReleases =
      releases.filter(
        (r) =>
          r.codename == device.variantOf &&
          !deviceReleases.some((dr) => dr.release == r.release)
      ) || [];

    variantReleases.forEach((release) => {
      deviceReleases.push({
        release: release.release
      });
    });

    device.releases = {
      current: null,
      suggested: null,
      default: null,
      all: deviceReleases.map((r) => {
        let isDefault = r.release == releasesData.default;
        let configRelease = releasesData.list.find((e) => e.name == r.release);
        return {
          path:
            "/device/" +
            slugify(device.codename, { decamelize: false }) +
            (!isDefault ? "/release/" + r.release : ""),
          deviceParam:
            slugify(device.codename, { decamelize: false }) +
            (!isDefault ? "/release/" + r.release : ""),
          default: isDefault,
          ...configRelease
        };
      })
    };

    device.releases.default =
      device.releases.all.find((r) => r.default) || null;

    if (deviceReleases.length == 0) {
      console.log(
        ansiCodes.redFG + "%s" + ansiCodes.reset,
        "Release file not provided for: " + device.codename
      );
      process.exit(2);
    }

    deviceReleases.forEach((release) => {
      // Copy device and merge data for each release
      let mergedDevice = JSON.parse(JSON.stringify(device));

      for (let key in release) {
        if (Object.keys(mergeFunctions.releases).includes(key))
          mergeFunctions.releases[key](mergedDevice, release, key);
        else
          mergeFunctions.releases.defaultMergeMethod(
            mergedDevice,
            release,
            key
          );
      }

      mergedDevice.releases.current = mergedDevice.releases.all.find(
        (r) => r.name == mergedDevice.release
      );
      mergedDevice.releases.suggested =
        mergedDevice.releases.all.find(
          (r) => r.name == mergedDevice.releases.current.supersededBy
        ) || null;
      mergedDevice.path = mergedDevice.releases.current.path; // Shorthand

      // Add release props to the schema
      mergedDevice.warnings = mergedDevice.warnings || {};
      if (mergedDevice.releases.current.dev) mergedDevice.warnings.dev = true;
      if (
        mergedDevice.releases.current.obsolete ||
        mergedDevice.releases.all.some(
          (r) => r.name == mergedDevice.releases.current.supersededBy
        )
      )
        mergedDevice.warnings.obsolete = true;
      if (mergedDevice.releases.current.hidden) mergedDevice.unlisted = true;
      mergedDevices.push(mergedDevice);
    });
  });
  return mergedDevices;
}

export function mergeVariants(devices) {
  return devices.map((device) => {
    if (!device.variantOf) return device;
    if (!device.name) device.unlisted = true;

    let deviceClone = JSON.parse(
      JSON.stringify(
        devices.find(
          (d) => d.codename == device.variantOf && d.release == device.release
        )
      )
    );
    for (let key in device) {
      if (Object.keys(mergeFunctions.variants).includes(key))
        mergeFunctions.variants[key](deviceClone, device, key);
      else mergeFunctions.variants.defaultMergeMethod(deviceClone, device, key);
    }
    return deviceClone;
  });
}

// Merge two arrays with concatenation
function mergeWithConcatenation(target, origin, key) {
  if (!target[key]) mergeWithDirectAssign(target, origin, key);
  else target[key].concat(origin[key]);
}

// Merge two objects with assignment
function mergeWithObjectAssign(target, origin, key) {
  if (!target[key]) mergeWithDirectAssign(target, origin, key);
  else Object.assign(target[key], origin[key]);
}

// Assign property to object
function mergeWithDirectAssign(target, origin, key) {
  target[key] = origin[key];
}

// Verify that the property is not an object or array, then merge
function verifiedDirectMerge(target, origin, key) {
  if (["array", "object"].includes(typeof origin[key])) {
    console.log(
      ansiCodes.redFG + "%s" + ansiCodes.reset,
      "Variant data cannot be merged for key: " + key
    );
    process.exit(2);
  }
  mergeWithDirectAssign(target, origin, key);
}

/*
 * Merge the device info table
 * If the target device doesn't have the table use the table from origin
 * If some specification is not present add it from originSpec
 * If it is not present in origin, keep it as it is
 * If it is present in both devices use originSpec
 */
function mergeDeviceInfo(target, origin, key) {
  if (!target[key]) target[key] = origin[key];
  origin[key].forEach((originSpec) => {
    let targetSpec = target[key].findIndex((s) => s.id == originSpec.id);
    if (targetSpec == -1) target[key].push(originSpec);
    else target[key][targetSpec] = originSpec;
  });
}

/*
 * Merge the feature support matrix
 * If portStatus doesn't exist on target make a deep copy from origin
 * Add feature / category from origin if it is not provided in target
 * Override feature / category from origin if it is available in both
 * Do nothing if the feature / category is only available in target
 */
function mergePortStatus(target, origin, key) {
  const originPortStatus = JSON.parse(JSON.stringify(origin[key]));
  if (!target[key]) target[key] = originPortStatus;
  else
    originPortStatus.forEach((originCategory) => {
      let targetCategory = target[key].findIndex(
        (c) => c.categoryName == originCategory.categoryName
      );
      if (targetCategory == -1) target[key].push(originCategory);
      else
        originCategory.features.forEach((originFeature) => {
          let targetFeature = target[key][targetCategory].features.findIndex(
            (f) => f.id == originFeature.id
          );
          if (targetFeature == -1)
            target[key][targetCategory].features.push(originFeature);
          else
            target[key][targetCategory].features[targetFeature] = originFeature;
        });
    });
}

// Verify that more specific content is not available and replace
function mergeContent(target, origin, key) {
  if (!origin[key]) return;
  mergeWithDirectAssign(target, origin, key);
}

// Debug function to compare without merging
function debugCompare(target, origin, key) {
  console.log("-- Debug comparison for " + key + " ---");
  console.log("Merging on: " + JSON.stringify(target[key]));
  console.log("from: " + JSON.stringify(origin[key]));
}
