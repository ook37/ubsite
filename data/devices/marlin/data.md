---
name: "Google Pixel XL"
deviceType: "phone"
image: "https://wiki.lineageos.org/images/devices/marlin.png"
subforum: "66/google-pixel-and-xl"

deviceInfo:
  - id: "cpu"
    value: "Quad-core 64-bit"
  - id: "chipset"
    value: "Qualcomm Snapdragon 821"
  - id: "gpu"
    value: "Qualcomm Adreno 530"
  - id: "rom"
    value: "32GB/128GB"
  - id: "ram"
    value: "4GB"
  - id: "android"
    value: "Android 7.1"
  - id: "battery"
    value: "2700 mAh"
  - id: "display"
    value: "AMOLED 1440x2560 5.5in"
  - id: "rearCamera"
    value: "12MP"
  - id: "frontCamera"
    value: "8MP"

contributors:
  - name: "Flohack74"
    forum: "https://forums.ubports.com/user/Flohack74"
    photo: "https://forums.ubports.com/assets/uploads/profile/414-profileavatar.png"

sources:
  portType: "external"

externalLinks:
  - name: "Device source"
    link: "https://github.com/Flohack74/android_device_google_marlin"
  - name: "Kernel source"
    link: "https://github.com/Flohack74/android_kernel_google_marlin"
---
